person = {
    'name': 'John Doe',
    'age' : 30,
    'address':'Nepal'
}
# print(person)
personViaConstructor= dict(first_name='John', last_name='Doe')
# print(personViaConstructor['first_name'])
person['age']=33
print(person.get('age'))
print (person.keys())
print (person.items())
#making a copy
person2=person.copy()

person2['city']='Kathmandu'
print (person2)
del person2['city']

person2.pop('first_name')

person.clear()
print (person)