class User:
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

    def greet(self):
        print(f'My name is {self.name}')


jon = User('jon', 'jon@gmail.com', 23)
jon.greet()