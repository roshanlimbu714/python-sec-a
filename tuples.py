fruTuple = ('Apple', 'Orange', 'Mango')

fruCon = tuple(('Apple', 'Orange', 'Mango'))
# tuples with one value should have trailing comma
fruSingle = ('apple',)
del fruSingle
# print (fruTuple + fruCon)
# print (len(fruSingle))

# sets are collection which is unordered and unindexed'

fruitSet = {'apple', 'orange', 'mango'}

fruiCon = set(('apple', 'orange', 'mango'))

# check if in a set
print('apple' in fruiCon)

# add to set
fruitSet.add('Grape')
fruitSet.remove('orange')
# to clear fruiCon.clear()
#del fruiCon to clear
print (fruitSet)
