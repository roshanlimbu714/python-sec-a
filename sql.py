import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="pystudy"
)
mycursor = mydb.cursor()
# mycursor.execute("CREATE TABLE Customers (name VARCHAR(255), address VARCHAR(255))")
#
# sql = "INSERT INTO Customers VALUES (%s,%s)"
# value = ('John', 'Paknajol')

# mycursor.execute(sql, value)

# mycursor.execute('Show tables')
#
# for x in mycursor:
# 	print(x)

mycursor.execute("Select * from Customers")

list = mycursor.fetchall()

for x in list:
    print(x)
